
## Terminology

#### Context

A data structure that is being queried.  In the context of this library it is a hyperscript VNode, but the same processes could be applied to lists, or any type of graph.

#### Action

A function that accepts a context and returns a context of the same type.
The action may or may not mutate the context.

#### Pure Action

An Action that does not mutate the input context.

#### Visitor

A unary function that accepts a context `<T>` and can return anything it wants (or nothing at all).

#### Pure Visitor

A Visitor that accepts and returns the same type and does not mutate the input.

#### Query

A Query is a curried function that accepts a visitor function and a context.
The only constraint within the function body is that the result of invoking the query returns a context of the same type as the input.

```js
visitor => context => {
    visitor( context )

    return context
}
```

A Query that has already been passed a Visitor, is called an Action

```
const query = visitor => context => {
    visitor(context)

    return context
}

const visitor = i => i
const action = query(visitor)
```

#### Pure Query

A Query that accepts a Pure Visitor

```js
pureVisitor => context => {
    return pureVisitor( context )
}
```

#### Traversal (Query)

A Query composed of other Queries

```js
visitor => context => query1(query2(query3(visitor)))(context)

// or $.compose(query1, query2, query3)
```

A Traversal query's signature is indistinguisable from any other query.  But it's helpful to have a name to identity a query pre-composed to target a particular section of a context.

E.g.

```js
const $traversal =
    $.compose( $root, $.child.first, $.child.all, $.attrs, $.className )

const visitor = $.class.add('red')
const action = $traversal(visitor)

action( rootVNode )
```
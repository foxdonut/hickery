#### Concepts from First Principles

A Query is a function...

```js
() => {}
```

A Query is a function that accepts a parent context...

```js
context => {}
```

A Query is function that visits a parent context...

```js
visitor => context => {
    return visitor(context.something)
}
```

A Query is a function that visits a parent context but *always* returns the original context, instead of the result of the function

```js
visitor => context => {
    visitor(context)

    // return context, not visitor(context)
    return context
}
```

Many utility belt libraries include a function that this library would consider a `Query`.

```js
function tap(f){
    return function(o){
        f(o)
        return o
    }
}
```

You'll see variants of this pattern in libraries like Ramda, Lodash, and Bluebird.

This function is usually used to opt out of a composition to do some side effects without interrupting the surrounding process.

```js
.then(f)
.tap( console.log )
.then( g )
```

```js
R.pipe(
    f
    , R.tap(console.log)
    , g
)
```

But `tap` is much more powerful than that.  The fact it always returns it's input, makes it *composeable*.

*`tap`* is the core abstraction of this entire library.

Imagine.  We have a nested object.

```js
var object = {
    a: {
        b: {
            c: {
                d: {

                }
            }
        }
    }
}
```


Imagine we want to add a property to `c` called `hello` with the value `world`.  And we also want to add a list to `d` with the key `cool`, containing the values `1,2,3,4`.  And then we also want to take that list and multiply each value by some number `n`.

First thing we want to do, is target `a`

```js
const query =
    tap(function(a){
        console.log('a', a)
    })

query(object)
// Logs: 'a' { a: { b: { c: { d: {} }}} }
// Returns: { a: { b: { c: { d: {} }}} }
```

Then next we want to do is access it's child property `b`.

```js
const query =
    tap(function(a){
        return tap(function(b){
            console.log('b', b)
        })(a.b)
    })

query(object)
// Logs: 'b' { b: { c: { d: {} }}}
// Returns: { a: { b: { c: { d: {} }}} }
```

Notice our query returns the complete object, but our log, is focused on a subset of that structure, that's important.

We'll be doing this often so let's create a query that accesses a sub property.

```js
function Prop(k){ // Query factory
    return function compose(f){ // Query
        return function execute(o){ // execute visitor: f
            f(o[k])
            return o
        }
    }
}
```

Or more succinctly

```js
const Prop = k => f => o => {
    f(o.k)
    return o
}
```

Now we can use that within our query proper.

```js
const $b = Prop('b')
const query =
    tap(
        $b( b => {
            console.log('b', b)
        })
    )
// Logs: 'b' { b: { c: { d: {} }}}
// Returns: { a: { b: { c: { d: {} }}} }
```

Using `Prop` lets us define queries for each property we will be encountering.

```js
const $b = Prop('b')
const $c = Prop('c')
const $d = Prop('d')
```

Or more succinctly

```js
const [$b, $c, $d] =
    ['b','c','d'].map( Prop )
```

And let's compose them together to create a query that traverses our object all the way down to `d`

```js
$tap($b($c($d(function(d){
    console.log('hello d', d)
}))))
// Logs: 'hello d' {}
// Returns: { a: { b: { c:{ d:{} }} }}
```

It's a little lispy, there's lots of parens.  But we'll take care of that later using `compose`.

Let's complete our task.

> Imagine we want to add a property to `c` called `hello` with the value `world`.  And we also want to add a list to `d` with the key `cool`, containing the values `1,2,3,4`.  And then we also want to take that list and multiply each value by some number `n`.

Adding a property to `c` called `hello`

```js
const addHelloToObjectAtC =
    $root( $a( $b( $c( c => c.hello = 'world' ))))
```

Adding a list to `d`

```js
const addListToObjectAtD =
    $root( $a( $b( $c( $d(function(d){
        d.cool = [1,2,3,4]
    })))))
```

Multiply the list by some value `n`

```js
const multiplyCoolByN =
    n => $root( $a( $b( $c( $d( function (d){
        d.cool = d.cool.map( x => x * n )
    })))))
```

All together now:

```js
addHelloToObjectAtC(object)
addListToObjectAtD(object)
multiplyCoolByN(3)(object)

console.log(object)

// Logs
// { a: { b: { c: { hello: 'world', d: { cool: [3,6,9] } } }}}
```

Because we wrote our queries to mutate the object, we could run them sequentially as separate statements.

But Queries need not mutate...

Earlier I said `tap` always returns the original object instead of the result of the transform.

But what if returning the original object wasn't the important part.  Maybe the important part is returning a context with the same structure as the input while transforming a subset.

```js
function Prop(k){
    return function(f){
        return function(o){
            return { ...o, [k]: f(o[k]) }
        }
    }
}
```

Let's try that code from earlier, but we'll make sure we never mutate the context.

```js
const [$a, $b, $c, $d, $hello, $cool] =
    ['a', 'b', 'c', 'd', 'hello', 'cool']
    .map( Prop )

const $objectC = $a($b($(c)))

const $objectD = $objectC( $d )

const addCoolToD =
    $objectD($hello( () => 'world' ))

const addListToC =
    $objectC( $cool([1,2,3]) )

const multiplyListByN = n => $objectC(
    $cool(
        xs => xs.map(n)
    )
)

multiplyListByN(
    addListToC(
        addCoolToD( object )
    )
)

// Returns
// { a: { b: { c: { hello: 'world', d: { cool: [3,6,9] } } }}}
```

But crucially, this version leaves the original object unmodified.

```
object
//=> { a: { b:{ c: { d: {} } }} }
```

So you can see Query's can be pure and impure.  But in both cases we can compose them to decouple an operation from knowledge of the larger structure.
